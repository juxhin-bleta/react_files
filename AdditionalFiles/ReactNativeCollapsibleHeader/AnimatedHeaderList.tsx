import React, { PureComponent, Fragment } from 'react';
import { View, TextInput, Text, Animated, ScrollView, FlatList, Dimensions } from 'react-native';
import CollapseHeader from './CollapseHeader';
import Global from '../../../modules/global'
import moment from 'moment';

/**
 * NOTE
 * By default this component is not using any Algolia's
 * connector. When used has to be wrapped with connectInfiniteHits().
 */

interface IAnimatedHeaderListProps {
  component?: any;
  children?: any;
  headerItems: {};
  headerRowsHeights: number[];
  fixedRow?: number;
  keyExtractor?: any;
  renderItem?: any;
  data: [];
  hits?: [];
  backgroundComponent?: any;
  backgroundGradient?: {};
  ListHeaderComponent?: any;
  emptyComponentMessage?: string;
}

interface IAnimatedHeaderListState {
  data: [];
  flatListAnimatedOpacity: any;
  fixedHeight: number;
}

const ANIMATION_DURATION = 300;

class AnimatedHeaderList extends PureComponent<IAnimatedHeaderListProps, IAnimatedHeaderListState> {


  static defaultProps = {
    component: FlatList,
    children: <View style={{ width: '100%', height: 1000 }} ></View>,
    headerItems: [
      <TextInput
        placeholder={'First Search'}
        style={{
          height: 80,
          backgroundColor: '#ccc'
        }}
      />,
    ],
    headerRowsHeights: [
      80,
    ],
    fixedRow: null,
    // key extractor for each list items
    keyExtractor: (item: any, index: number) => item.id,
    // Place your custom component here
    renderItem: ({ item, index }: any) => (
      <View>
        <Text
          id={index}
          style={{ borderBottomColor: '#666666', borderBottomWidth: 1, height: 90, backgroundColor: 'red' }}
        >{item.key}
        </Text>
      </View>
    ),
    data: [],
    backgroundComponent: null,
    backgroundGradient: {},
  }

  public constructor(props: any) {
    super(props);
    this.state = {
      data: this.props.data,
      flatListAnimatedOpacity: new Animated.Value(0),
      fixedHeight: 0
    }
  }

  componentDidMount() {
    Animated.timing(
      this.state.flatListAnimatedOpacity, {
        toValue: 1,
        duration: ANIMATION_DURATION,
        useNativeDriver: true,
      }
    ).start()
  }

  async componentDidUpdate() {
    if (this.props.hits && this.state.data !== this.props.hits) {
      console.log(this.props.hits, 'hits update');
      await this.setState({
        data: this.props.hits,
        flatListAnimatedOpacity: new Animated.Value(0)
      })
      Animated.timing(
        this.state.flatListAnimatedOpacity, {
          toValue: 1,
          duration: ANIMATION_DURATION,
          useNativeDriver: true,
        }
      ).start()
    } else {
      return false
    }
  }

  // function that calculates the sum of each header row
  public getSumOfHeaderRows = (arg = []) => {
    return arg.reduce((sum, value) => sum + value, 0);
  }
  // initialize Animated value and simple Y move
  animatedScrollY = new Animated.Value(0);

  // set animation value to pass it to the header
  animation = this.animatedScrollY

  public _onScroll = async (event: any) => {
    Animated.event(
      [
        {
          nativeEvent: { contentOffset: { y: this.animatedScrollY } },
        },
      ]
    )(event)
    this.props.onScroll && this.props.onScroll(event)
  }

  public _onScrollBeginDrag = async (event: any) => {
    this.initialOffsetY = event.nativeEvent.contentOffset.y
    this.props.onScrollBeginDrag && this.props.onScrollBeginDrag(event)
  }

  public _onScrollEndDrag = (event: any) => {
    this._scrollableAreaMove(event.nativeEvent.contentOffset.y, event);
    this.props.onScrollEndDrag && this.props.onScrollEndDrag(event)
  }

  public _getClampIndex = (scrollY: any, baseIndex = 0) => {
    let index = 0
    let sum = 0;
    while (sum < scrollY && index >= 0) {
      sum += this.props.headerRowsHeights[index];
      index++;
    }
    if (index > this.props.headerRowsHeights.length) {
      return -1;
    }
    return index + baseIndex;
  }

  public _getSumBeforeClampIndex(currentIndex: any) {
    return this.props.headerRowsHeights.reduce((sum, height, index) => {
      if (index < currentIndex) {
        return sum + height;
      }
      return sum;
    }, 0)
  }

  // method that handles automatic moves of header
  public _scrollableAreaMove = (scrollY: any, event: any) => {
    const ListComponent = this.props.component
    const offsetY = scrollY - this.initialOffsetY;
    const clampIndex = this._getClampIndex(scrollY, offsetY < 0 ? -1 : 0)

    // TODO: Need to fix, lines below were added to fix behavior
    // if the fixed element is not the last one.
    const { headerRowsHeights, fixedRow } = this.props
    let ignoredDistance = 0;
    if (fixedRow != null && fixedRow != headerRowsHeights.length - 1) {
      ignoredDistance = headerRowsHeights[fixedRow]
    }

    if (clampIndex !== -1) {
      const sumAfterClampIndex = this._getSumBeforeClampIndex(clampIndex)
      if (ListComponent === ScrollView) {
        this.scrollView.scrollTo({ x: 0, y: sumAfterClampIndex - ignoredDistance, animated: true })
      } else {
        this.scrollView.scrollToOffset({ offset: sumAfterClampIndex - ignoredDistance, animated: true })
      }
    }
  }

  /**
   * Method to edit body height in order to always enable animation.
   * TODO: Apply also for flat list
   * Working only for ScrollView
   */
  public handleBodyHeight = (event: any) => {
    if (event.nativeEvent.layout.height < Dimensions.get('window').height) {
      this.setState({
        fixedHeight: Dimensions.get('window').height - this.props.headerRowsHeights[this.props.fixedRow]
      })
    }
  }

  public _handleComponentType = () => {
    const ListComponent = this.props.component;
    const {
      headerRowsHeights, ListHeaderComponent, children,
      backgroundComponent, backgroundGradient
    } = this.props;

    if (ListComponent === ScrollView) {
      return (
        <ListComponent
          style={{ zIndex: 1 }}
          onScroll={this._onScroll}
          onScrollBeginDrag={this._onScrollBeginDrag}
          onScrollEndDrag={this._onScrollEndDrag}
          ref={(ref) => {
            this.scrollView = ref
            this.props.innerRef && this.props.innerRef(ref)
          }}
          showsVerticalScrollIndicator={
            backgroundComponent === null ? true : false
          }
          scrollEventThrottle={this.props.scrollEventThrottle || 16}
        >
          <Fragment>
            <View style={{
              height: this.getSumOfHeaderRows(headerRowsHeights),
              zIndex: -1
            }}
            />
            {ListHeaderComponent && <ListHeaderComponent  {...listHeaderProps} />}
          </Fragment>
          <View
            onLayout={
              (event) => { this.handleBodyHeight(event) }
            }
            // TODO: Fix this in a later stage , { height: this.state.fixedHeight }
            style={[children && this.state.fixedHeight !== 0 ? null : null, { flex: 1 }]}>
            {
              children ||
              <View style={[{ padding: 30, backgroundColor: 'white' }]}>
                <Global.EmptyList message={this.props.emptyComponentMessage || null} />
              </View>
            }
          </View>
        </ListComponent>
      );
    } else {
      return (
        <Animated.View style={{ flex: 1, opacity: this.state.flatListAnimatedOpacity }}>
          <FlatList
            style={{ zIndex: 1 }}
            // TODO: Fix the key
            keyExtractor={(item, index) => index.toString()}
            onScroll={this._onScroll}
            onScrollBeginDrag={this._onScrollBeginDrag}
            onScrollEndDrag={this._onScrollEndDrag}
            ref={(ref) => {
              this.scrollView = ref
              this.props.innerRef && this.props.innerRef(ref)
            }}
            showsVerticalScrollIndicator={
              backgroundComponent === null ? true : false
            }
            scrollEventThrottle={this.props.scrollEventThrottle || 16}
            ListHeaderComponent={(listHeaderProps) => {
              return (
                <Fragment>
                  <View style={{
                    height: this.getSumOfHeaderRows(headerRowsHeights),
                    zIndex: -1
                  }}
                  />
                  {ListHeaderComponent && <ListHeaderComponent  {...listHeaderProps} />}
                </Fragment>
              )
            }}
            ListEmptyComponent={
              <View style={{ flex: 1, paddingTop: '35%', paddingHorizontal: 30 }}>
                <Global.EmptyList message={this.props.emptyComponentMessage || null} />
              </View>}
            {...this.props}
            data={this.props.hits ? this.state.data : null}
          />
        </Animated.View>
      );
    }
  }

  public render() {
    const { headerRowsHeights, fixedRow, headerItems, backgroundComponent, backgroundGradient } = this.props

    return (
      <View style={{ flex: 1 }}>
        {this._handleComponentType()}
        <CollapseHeader
          scrolledDistanceY={this.animation}
          heights={headerRowsHeights}
          fixed={fixedRow}
          items={headerItems}
          backgroundComponent={backgroundComponent}
          backgroundGradient={backgroundGradient}
        />
      </View>
    );
  }
}

export default AnimatedHeaderList;