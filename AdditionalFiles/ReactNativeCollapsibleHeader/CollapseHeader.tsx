import React, { Component } from 'react';
import { View, Animated } from 'react-native';

interface Props {
  scrolledDistanceY: any,
  heights: [],
  fixed: number
}

class CollapseHeader extends Component<Props> {

  static defaultProps = {
    backgroundComponent: null,
    backgroundGradient: {},
    scrolledDistanceY: new Animated.Value(0),
  }

  public _animatedItem = (index: any) => {
    const { scrolledDistanceY, heights, fixed } = this.props;
    // Total height
    const totalHeight = heights.reduce((s, h) => s + h, 0);

    const fixedTransform = index === fixed ? heights[index] : 0;
    const moveTransform = heights.filter((h, i) => i <= index).reduce((s, h) => s + h, 0);
    // const moveTransform = heights[index]

    return (
      {
        transform: [{
          translateY: scrolledDistanceY.interpolate({
            inputRange: [0, 0, moveTransform - fixedTransform],
            outputRange: [0, 0, -(moveTransform - fixedTransform)],
            extrapolate: 'clamp',
          })
        }],
        // opacity:scrolledDistanceY.interpolate({
        //   inputRange: [0, 0, moveTransform - fixedTransform],
        //   outputRange: [1, 1, 0],
        //   extrapolate: 'clamp',
        // })
      }
    );
  }

  render() {
    return (
      <Animated.View
        style={[
          {
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%',
            justifyContent: 'flex-end',
          },
        ]}
      >
        <View>
          {
            this.props.items.map(
              (item, index) => {
                return (
                  <Animated.View
                    key={index}
                    style={[
                      // TODO: Needs a better solution
                      { zIndex: this.props.heights.length - index },
                      this._animatedItem(index),
                      index == this.props.fixed ? { opacity: 1 } : false
                    ]}
                  >
                    {item}
                    {/* {
                      // TODO: Add the background for the fixed element in case there is
                      // a background component and gradient
                      
                      this.props.backgroundGradient && this.props.fixed ?
                      <View 
                        style={[{
                            position:'absolute',
                            left: 0,
                            top:0,
                            width: '100%',
                            height: '100%',
                            zIndex:-1},
                          this.props.backgroundGradient
                        ]}>
                      </View>
                      : false
                    } */}
                  </Animated.View>
                )
              }
            )
          }
        </View>

        {this.props.backgroundComponent != {} ?
          <Animated.View
            style={[{
              position: 'absolute',
              left: 0,
              top: 0,
              width: '100%',
              height: '100%',
              zIndex: -2
            }
            ]}>
            {this.props.backgroundComponent}
          </Animated.View>
          : false
        }
        {
          this.props.backgroundGradient != null ?
            <View
              style={[{
                position: 'absolute',
                left: 0,
                top: 0,
                width: '100%',
                height: '100%',
                zIndex: -1
              },
              this.props.backgroundGradient
              ]}>
            </View>
            : false
        }
      </Animated.View>
    )
  }
}

export default CollapseHeader;
