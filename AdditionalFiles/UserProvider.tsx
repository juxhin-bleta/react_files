import React, { createContext, useEffect, useState, useRef, Dispatch } from 'react';
//
import { UserInfo } from '../PropsTypes/UserTypes'
//
import { Core, Api, ResponseStatus } from '../Services';
import { GeneralLoader, GeneralToastNotification } from '../Components';
import strLng from '../localization/strLng';
import ReportProblemModal from '../Components/ReportProblemModal';
import { NativeModules, Alert } from 'react-native';

interface IUserProviderProps {
	children: JSX.Element
}

interface IUserContextTypes {
	isUserLogged: boolean | any,
	setIsUserLogged: Dispatch<boolean>;
	userToken: string | any;
	setUserToken: Dispatch<string>;
	userInfo: UserInfo | any;
	setUserInfo: Dispatch<UserInfo>;
	platformType: string;
	DrawerRef: any;
	generalMessage: (arg: string, timeout?: number) => void;
	generalLoader: any;
	setGeneralLoader: Dispatch<boolean>;
	pendingOrderData: any[];
	setPendingOrderData: Dispatch<any>;
	onwayOrderData: any[];
	setOnwayOrderData: Dispatch<any>;
	isShiftStarted: boolean;
	setIsShiftStarted: Dispatch<boolean>;
	ordersFilterDate: any;
	setOrdersFilterDate: Dispatch<any>;
	restaurantInfo: any;
	isReportModalVisible: boolean;
	setIsReportModalVisible: Dispatch<boolean>;
	reportProblemMain: (path: string, error: object) => void;
	selectedLanguage: boolean,
	setSelectedLanguage: (arg: string) => void;
	refreshOrders: boolean;
	setRefreshOrders: (arg: boolean) => void;
}

const USER_CONTEXT_INITIAL_VALUES = {
	isUserLogged: false,
	setIsUserLogged: (arg: boolean) => undefined,
	userToken: null,
	setUserToken: (arg: string) => undefined,
	userInfo: null,
	setUserInfo: (arg: UserInfo) => undefined,
	platformType: '',
	DrawerRef: null,
	generalMessage: (arg: string, timeout?: number) => undefined,
	generalLoader: null,
	setGeneralLoader: (arg: boolean) => undefined,
	pendingOrderData: [],
	setPendingOrderData: (arg: []) => undefined,
	onwayOrderData: [],
	setOnwayOrderData: (arg: []) => undefined,
	isShiftStarted: false,
	setIsShiftStarted: (arg: boolean) => undefined,
	ordersFilterDate: new Date(),
	setOrdersFilterDate: (arg: any) => undefined,
	restaurantInfo: null,
	isReportModalVisible: false,
	setIsReportModalVisible: (arg: boolean) => undefined,
	reportProblemMain: (path: string, error: object) => undefined,
	selectedLanguage: null,
	setSelectedLanguage: (arg: string) => undefined,
	refreshOrders: false,
	setRefreshOrders: (arg: boolean) => undefined,
};

export const UserContext = createContext<IUserContextTypes>({ ...USER_CONTEXT_INITIAL_VALUES });

const UserProvider = ({ children }: IUserProviderProps) => {

	console.disableYellowBox = true;

	const [isUserLogged, setIsUserLogged] = useState<boolean | any>(null);
	const [userToken, setUserToken] = useState<string | any>(null);
	// TODO: Fix Typos for "UserInfo"
	const [userInfo, setUserInfo] = useState<UserInfo | any>(null);
	// TODO: Fix Typos for "RestaurantInfo"
	const [restaurantInfo, setRestaurantInfo] = useState<any>(null);

	const [ordersFilterDate, setOrdersFilterDate] = useState<any>(new Date());
	const [isShiftStarted, setIsShiftStarted] = useState<boolean>(false);

	const [refreshOrders, setRefreshOrders] = useState<boolean>(false)
	const [pendingOrderData, setPendingOrderData] = useState<any[]>([]);
	const [onwayOrderData, setOnwayOrderData] = useState<any[]>([]);

	// TODO: Separate in the future in another context to improve current context readability
	const [toastVisibiity, setToastVisibility] = useState<boolean>(false);
	const [toastMessage, setToastMessage] = useState<string>('');
	const [generalLoader, setGeneralLoader] = useState<boolean>(false);
	const [platformType, setPlatformType] = useState<string>('');
	const DrawerRef = useRef<any>(null);
	const [isReportModalVisible, setIsReportModalVisible] = useState<boolean>(false)
	const [reportModalInfo, setReportModalInfo] = useState<object | null>(null)
	const [selectedLanguage, setSelectedLanguage] = useState<string | null>(null);

	const localization = NativeModules.ReactLocalization;


	useEffect(
		() => {
			Core.checkAuth().then(
				(res) => {
					if (res) {
						console.log(res);
						Core.getUserInfoLoc().then(
							(user) => {
								if (user) {
									console.log(user);
									setUserToken(res);
									setIsUserLogged(true);
									setUserInfo(JSON.parse(user));
								}
							}
						)
					} else {
						setIsUserLogged(false);
					}
				}
			).catch(
				() => { setIsUserLogged(false); }
			);

			if (localization) {
				setSelectedLanguage(languageFormater(localization.language));
			}
		}
		, []
	);

	useEffect(() => {
		if (userInfo) {
			let newUser = { ...userInfo, on_shift: isShiftStarted }
			if (JSON.stringify(userInfo) !== JSON.stringify(newUser)) {
				Core.setUserInfoLoc(JSON.stringify(newUser)).then(
					() => { setUserInfo(newUser); }
				).catch(
					(err) => {
						console.log(err);
					}
				)
			}
		}
	}, [isShiftStarted]);

	useEffect(() => {

		if (userInfo) {
			setIsShiftStarted(userInfo.on_shift);
			if (!restaurantInfo) {
				getRestaurantInfo(userInfo.network_id, userInfo.restaurant_id);
			}
		}
	}, [userInfo]);

	// reset modal info in close
	useEffect(() => {
		if (!isReportModalVisible) {
			setReportModalInfo(null)
		}
	}, [isReportModalVisible]);

	function getRestaurantInfo(network_id: number, restaurant_id: number) {
		setGeneralLoader(true)
		const path = `/network/${network_id}/restaurant/${restaurant_id}`
		Api.get(path).then(
			
			(response: any) => {
				let responseStatus = ResponseStatus.handleResponse(response)
				if (response.status !== 401) {

				if (responseStatus === true) {
					console.log(response.data);
					setRestaurantInfo(response.data)
					setGeneralLoader(false)
				} else {
					Alert.alert(
						strLng.LABELS.warning,
						strLng.ERROR.problem_retrieving_restaurant_info,
						[
							{
								text: strLng.ACTIONS.report,
								onPress: () => reportProblemMain(`${'{get}'}${path}`, JSON.stringify(response.data))
							},
							{ text: strLng.ACTIONS.retry, onPress: () => getRestaurantInfo(network_id, restaurant_id) },
						],
						{ cancelable: true },
					);
					setGeneralLoader(false);
				}
			}else{
				Core.removeToken()
				setIsUserLogged(false)
			}
			}).catch(
				() => {
					generalMessage(strLng.ERROR.error)
					setGeneralLoader(false)
					let fullPath = `${"{get}"}${path}`
				}
			);
	}

	function reportProblemMain(path: string, error: string) {
		setIsReportModalVisible(true)
		setReportModalInfo(
			{
				path: path,
				error: error
			}
		)
	}

	const generalMessage = (arg: string = strLng.ERROR.error, timeout: number = 2000) => {
		setToastVisibility(true)
		setTimeout(
			() => { setToastVisibility(false); }
			, timeout
		)
		setToastMessage(arg);
	}

	function languageFormater (arg:string) {
		switch (arg) {
			case 'en-US':
			return "English - US"
		
			default:
			return "English - US"
		}
	}

	const providerValue = {
		isUserLogged, setIsUserLogged, userToken, setUserToken, userInfo, setUserInfo,
		restaurantInfo,
		pendingOrderData, setPendingOrderData, onwayOrderData, setOnwayOrderData,
		isShiftStarted, setIsShiftStarted,
		ordersFilterDate, setOrdersFilterDate,

		platformType,
		DrawerRef,
		generalMessage,
		generalLoader, setGeneralLoader,
		isReportModalVisible, setIsReportModalVisible, reportProblemMain,
		selectedLanguage, setSelectedLanguage,
		refreshOrders, setRefreshOrders
	}

	return (
		<UserContext.Provider value={providerValue}>
			{children}
			<GeneralToastNotification
				visibility={toastVisibiity}
				message={toastMessage}
			/>
			<GeneralLoader />
			<ReportProblemModal
				info={reportModalInfo}
			/>
		</UserContext.Provider>
	)
}

export default UserProvider