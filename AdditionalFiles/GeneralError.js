import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Snackbar,
  SnackbarContent,
  Modal,
  CircularProgress
} from "@material-ui/core";
import { connect } from "react-redux";
// import actions
import {
  editIsLoadingFullScreenState,
  editErrorState
} from "../../actions/index";

class GeneralMessage extends Component {
  state = {
    bottomSnackVertical: "bottom",
    bottomSnackHorizontal: "center"
  };

  handleSnackBarContent = () => {
    const { msgType, msg } = this.props.error;

    return (
      <SnackbarContent
        className={msgType == "success" ? "bg-green" : "bg-red"}
        message={
          <div
            className={"w-100 d-flex justify-content-center align-items-center"}
          >
            <p className={"text-white text-center p-2"}>{msg}</p>
          </div>
        }
      ></SnackbarContent>
    );
  };

  modalClose = async () => {
    await this.props.editIsLoadingFullScreenState(false);
  };

  componentDidUpdate(prevProps) {
    if (prevProps != this.props) {
      this.setState({ open: true });
    }
  }

  render() {
    const vertical = this.state.bottomSnackVertical;
    const horizontal = this.state.bottomSnackHorizontal;
    return (
      <div>
        <Snackbar
          className={
            this.props.error.msgType == "success"
              ? "bg-green rounded"
              : "bg-red rounded"
          }
          anchorOrigin={{ vertical, horizontal }}
          key={`${vertical},${horizontal}`}
          open={this.props.error.open}
          onClose={this.props.editErrorState.bind(this, { open: false })}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
        >
          {this.handleSnackBarContent()}
        </Snackbar>
        <Modal
          className={"d-flex justify-content-center align-items-center"}
          onClose={this.modalClose.bind(this, false)}
          open={this.props.isLoadingFullScreen}
        >
          <div className={"p-5 no-outline"}>
            <CircularProgress className={"text-theme no-outline-focus"} />
          </div>
        </Modal>
      </div>
    );
  }
}

GeneralMessage.propTypes = {
  isLoadingFullScreen: PropTypes.bool,
  error: PropTypes.object,

  editIsLoadingFullScreenState: PropTypes.func,
  editErrorState: PropTypes.func
};

const mapStateToProps = state => {
  const { isLoadingFullScreen, error } = state.initialState;
  return {
    isLoadingFullScreen,
    error
  };
};

export default connect(
  mapStateToProps,
  {
    editIsLoadingFullScreenState,
    editErrorState
  }
)(GeneralMessage);
