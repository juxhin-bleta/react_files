import React, { ReactText, CSSProperties, useState, useEffect } from 'react'
import { Spinner } from './utility/GlobalLoader'

interface InputBoxProps {
	id: string;
	name?: string;
	type: string;
	value?: string | number | ReactText | null;
	required?: boolean;
	style?: CSSProperties | any;
	className?: string | any;
	label?: string | number | ReactText;
	labelStyle?: CSSProperties | any;
	labelClassName?: string | any;
	inputStyle?: CSSProperties | any;
	inputClassName?: string | any;
	error?: string | null;
	errorStyle?: CSSProperties | any;
	errorClassName?: string | any;
	isLoading?: boolean;
	isValid?: boolean | null;
	hasValidationIndicator?: boolean,
	onChange: (value: string | number) => void;
}

/**
 * @param id             Inputs HTML ID which is also being used for the label, is marked as required to prevent
 *                       the component from have a label without ID
 * @param type           Input's HTML type
 * @param name           Possibly Formik prop
 * @param value          Input's value
 * @param required       Input's HTML validation, default to false
 * @param style          Wrapper's style
 * @param className      Wrapper's className
 * @param label          Input's label
 * @param labelStyle     If Label, label's style(object) provided from parent
 * @param labelClassName If Label, label's HTML class(style) provided from parent
 * @param labelStyle     Input's style(object) provided from parent
 * @param labelClassName Input's HTML class(style) provided from parent
 * @param error          Error paragraph element
 * @param errorStyle     If error, label's style(object) provided from parent
 * @param errorClassName If error, label's HTML class(style) provided from parent
 * @param isLoading      To weather display the side loader or not.
 * @param isValid        To weather display in indicator if the field is valid or not.
 * @param hasValidationIndicator To adapt style for the validation indicator
 * @param onChange       Function to uplift the input's value
 */

const InputBox = ({
	id,
	type,
	value,
	required = false,
	style,
	className,
	label,
	labelStyle,
	labelClassName,
	inputStyle,
	inputClassName,
	error,
	errorStyle,
	errorClassName,
	isLoading,
	isValid,
	hasValidationIndicator,
	onChange,
	...props
}: InputBoxProps) => {

	const [inputValue, setInputValue] = useState<any>('')

	useEffect(() => {
		onChange(inputValue)
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [inputValue])

	return (
		<div
			style={
				Object.assign(
					{},
					(style ? style : {})
				)
			}
			className={`flex flex-col pt-3 ${className ? className : ''}`}
		>
			<div className={'flex flex-col-reverse relative w-full relative'}>

				{/* Input HTML Element */}
				<input
					onChange={(event) => setInputValue(event.target.value)}
					id={id}
					type={type}
					aria-required={required ? true : false}
					style={
						Object.assign(
							{},
							(inputStyle ? inputStyle : {})
						)
					}
					className={`g-white border-2 border-gray-500 focus:border-black input-text outline-none p-2 rounded-lg text-left w-full ${inputClassName ? inputClassName : ''} ${hasValidationIndicator ? 'pr-8' : ''}
          `}
					{...props}
				/>
				{/* End Input HTML Element */}

				{/* Label HTML Element */}
				{
					label ?
						<label
							htmlFor={id}
							style={
								Object.assign(
									{},
									(inputValue && inputValue !== '' ? { top: -13 } : {}),
									(labelStyle ? labelStyle : {})
								)
							}
							className={`px-2 text-gray-700 ${labelClassName ? labelClassName : ''} ${inputValue && inputValue !== '' ? '' : 'text-gray-500'}`}
						>
							{
								required ?
									<span
										className={'text-red-800 mr-1'}
									>*</span>
									: null
							}
							{label}
						</label>
						: null
				}
				{/* End Label HTML Element */}
				{
					hasValidationIndicator ?
						<div className={'absolute right-0 top-0 p-2'} style={{ minWidth: 35 }}>
							{
								isLoading ?
									<Spinner size={30} color={'rgb(93, 93, 93)'} />
									: isValid === true ?
										<div><p style={{ fontSize: 20, color: 'green' }}>&#10004;</p></div> :
										isValid === false ?
											<div><p style={{ fontSize: 20, color: 'red' }}>&times;</p></div>
											: null
							}
						</div>
						: null
				}
			</div>

			{/* Error Paragraph HTML Element */}
			{
				error ?
					<p
						style={
							Object.assign(
								{},
								(errorStyle ? errorStyle : {})
							)
						}
						className={`js-mediaworld-input-error text-red-800 text-xs px-2 ${errorClassName ? errorClassName : ''}`}
					>
						{error}
					</p>
					: null
			}
			{/* End Error Paragraph HTML Element */}
		</div>
	)
}

export default InputBox
