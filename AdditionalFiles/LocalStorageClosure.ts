class MainInfo {

    public storage: any = null;
  
    constructor() {
      this.storage = this.checkStorageType();
    }
    // Test if local storage is possible to use
    checkStorageType = () => {
      let st = null;
      try {
        localStorage.setItem("test", "test");
        localStorage.removeItem("test");
        st = localStorage;
      } catch (e) {
        st = sessionStorage;
      }
      return st;
    };
  
    setUserToken = (arg: string) => {
      try {
        this.storage.setItem("token", arg);
        return true
      } catch (error) {
        return false
      }
    };
  
    getUserToken = () => {
      try {
        this.storage.getItem("token");
        return true
      } catch (error) {
        return false
      }
    };
  
    deleteUserToken = () => {
      try {
        this.storage.removeItem("token");
        return true
      } catch (error) {
        return false
      }
    };
  
    checkAuth = () => {
      try {
        let token = this.storage.getItem("token");
        if (token != null) {
          return token;
        } else {
          return false;
        }
      } catch (e) {
        return false;
      }
    };
  
  }
  
  let State = new MainInfo();
  
  export default State;
  