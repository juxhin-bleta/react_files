import React from 'react';
import { View } from 'react-native';
import Text from '../Text';
import { Colors } from '../../utils'

interface IPriceBoxProps {
  amount: number | string;
  style?: {};
  increment?: boolean;
  fontStyle?: {};
  reverse?: boolean;
}

const PriceBox = ({ amount, style, increment, fontStyle, reverse }: IPriceBoxProps) => {
  return (
    <View
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center'
        },
        style
      ]}
    >
      {
        increment ?
          <Text style={[ fontStyle ]}>+ </Text>
          : false
      }
      <View
        style={
          reverse ? {
            flexDirection: 'row-reverse'
          }
          : {
            flexDirection: 'row'
          }
        }
      >
        <Text style={[ fontStyle ]}>$</Text>
        <Text style={[ fontStyle ]}>{amount}</Text>
      </View>
    </View>
  )
}

PriceBox.defaultProps = {
  style: {},
  increment: false,
  fontStyle: {color:'black'},
  reverse: false,
}

export default PriceBox