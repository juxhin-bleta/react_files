import React, { Component } from "react";
import { TextField, Button } from "@material-ui/core";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Formik } from "formik";
import * as yup from "yup";
//import components
import BasicLayout from "./layouts/BasicLayout";
import NavMenu from "./screen_components/NavMenu";
//
import { Api, ResponseStatus, Core } from "../../services/index";
// import actions
import {
  editAuthState,
  editErrorState,
  editIsLoadingFullScreenState
} from "../../actions/index";
// import language
import strLng from "../../localization/strLng";

class LoginScreen extends Component {
  state = {
    formikInitialSubmit: false
  };

  handleFormSubmit = values => {
    if (!this.state.formikInitialSubmit) {
      this.setState({ formikInitialSubmit: true });
    }
    const { email, password } = values;
    //form the object with data
    let data = new FormData();
    data.append("network_id", Api.getNetworkId());
    data.append("username", email);
    data.append("password", password);

    //check password input status if different from error
    this.props.editIsLoadingFullScreenState(true);
    //after checking all the fields do the request
    Api.postNoAuth("/login", data)
      .then(response => {
        let responseStatus = ResponseStatus.handleResponse(response);
        if (responseStatus === true) {
          Core.setUserToken(response.data.token);
          this.props.editAuthState(true);
          this.props.editErrorState({
            open: true,
            type: "snack",
            msgType: "success",
            msg: strLng.MESSAGE_login_successful
          });
          this.props.editIsLoadingFullScreenState(false);
          this.props.history.goBack();
        } else if (responseStatus) {
          this.props.editErrorState({
            open: true,
            type: "snack",
            msgType: "error",
            msg: responseStatus
          });
          this.props.editIsLoadingFullScreenState(false);
        }
      })
      .catch(() => {
        this.props.editErrorState({
          open: true,
          type: "snack",
          msgType: "error",
          msg: strLng.ERROR.error
        });
        this.props.editIsLoadingFullScreenState(false);
      });
  };

  render() {
    return (
      <BasicLayout>
        <div className={"d-flex flex-column min-h-100vh"}>
          <div className={["login-screen container-fluid ows p-0 bg-theme"]}>
            <div className={["row flex-column iw px-0 w-100"]}>
              <div className={"col-12 p-0"}>
                <NavMenu screen={"back"} />
              </div>
            </div>
          </div>
          <div className={["login-screen container-fluid flex-grow-1 ows p-0"]}>
            <div
              className={[
                "row flex-column flex-grow-1 align-items-center sw px-0 w-100 bg-white"
              ]}
            >
              <div
                className={
                  "d-flex justify-content-center xsw flex-grow-1 flex-column p-3 col-12"
                }
              >
                <Formik
                  initialValues={{
                    email: "",
                    password: ""
                  }}
                  validationSchema={yup.object().shape({
                    email: yup
                      .string()
                      .email(strLng.ERROR.email_invalid)
                      .required(strLng.ERROR.email_required),
                    password: yup
                      .string()
                      .nullable()
                      .required(strLng.ERROR.password_required)
                  })}
                  onSubmit={values => this.handleFormSubmit(values)}
                  validateOnChange={this.state.formikInitialSubmit}
                >
                  {props => {
                    return (
                      <form className={"d-flex flex-column"}>
                        <TextField
                          name={"email"}
                          id="input-email"
                          type="text"
                          label={strLng.email}
                          className={"m-2"}
                          placeholder={strLng.email}
                          value={props.values.email}
                          onChange={props.handleChange("email")}
                          margin="normal"
                          error={props.errors.email ? true : false}
                        />
                        {props.errors.email ? (
                          <p className={"text-red text-small pl-2"}>
                            {props.errors.email}
                          </p>
                        ) : null}
                        <TextField
                          name={"password"}
                          id="input-password"
                          type="password"
                          label={strLng.password}
                          className={"mx-2 mt-2"}
                          placeholder={strLng.password}
                          value={props.values.password}
                          onChange={props.handleChange("password")}
                          margin="normal"
                          error={props.errors.password ? true : false}
                        />
                        {props.errors.password ? (
                          <p className={"text-red text-small pl-2"}>
                            {props.errors.password}
                          </p>
                        ) : null}
                        <Button
                          onClick={() => props.handleSubmit()}
                          className={"bg-blue mb-1 mt-3"}
                        >
                          <p className={"text-white text-capitalize"}>
                            {strLng.login}
                          </p>
                        </Button>
                      </form>
                    );
                  }}
                </Formik>

                <div className={"bg-transparent mb-2"}>
                  <p className={"text-blue text-center text-capitalize"}>
                    {strLng.forgot_password}
                  </p>
                </div>
                <div className={"d-flex align-items-center mb-2"}>
                  <div className={"side-separator flex-grow-1"} />
                  <p className={"mx-3 text-gray"}>{strLng.or}</p>
                  <div className={"side-separator flex-grow-1"} />
                </div>
                <Button className={"bg-fb mb-3"}>
                  <img
                    className={"icon-img-sm p-1 mr-1"}
                    src={"../../images/logo/facebook.svg"}
                  />
                  <p className={"text-white text-capitalize"}>
                    {strLng.sign_in_with_facebook}
                  </p>
                </Button>
                <Button className={"bg-google"}>
                  <img
                    className={"icon-img-sm mr-1"}
                    src={"../../images/logo/google-plus.svg"}
                  />
                  <p className={"text-white text-capitalize"}>
                    {strLng.sign_in_with_google}
                  </p>
                </Button>
                <div
                  className={
                    "d-flex align-items-center justify-content-center mt-4"
                  }
                >
                  <p className={"text-gray mr-1"}>{strLng.sign_up_label}</p>
                  <Button onClick={() => this.props.history.push("/register")}>
                    <span className={"text-capitalize"}>
                      {strLng.ACTION.sign_up}
                    </span>
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </BasicLayout>
    );
  }
}

LoginScreen.propTypes = {
  history: PropTypes.object,
  authenticated: PropTypes.bool,

  editAuthState: PropTypes.func,
  editErrorState: PropTypes.func,
  editIsLoadingFullScreenState: PropTypes.func
};

const mapStateToProps = state => {
  const { authenticated } = state.initialState;
  return {
    authenticated
  };
};

export default connect(
  mapStateToProps,
  {
    editAuthState,
    editErrorState,
    editIsLoadingFullScreenState
  }
)(LoginScreen);
