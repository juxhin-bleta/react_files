import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Dimensions } from 'react-native';
import { TouchableItem } from '../../global/components/Button';
import { Colors, Metrics, Fonts } from '../../global/utils/index';
import { Placeholder, PlaceholderLine, ShineOverlay } from "rn-placeholder";
/**
 * NOTE
 * By default this component is not using any Algolia's
 * connector. When used has to be wrapped with connectMenu().
 */

export interface CustomTabMenuItem{
  id: number;
  label: string;
}

interface ICustomTabsState {
  selectedTabId: number | null;
  tabWidths: number[];
  dataSelected: object[] | any;
}

interface ICustomTabsProps {
  type?: string;
  data?: object[];
  dataTypeBusiness?: [];
  dataTypeDynamic?: [];
  height?: number;
  tabsWrapStyle?: object;
  tabStyle?: object;
  selectedTabStyle?: object;
  tabTextStyle?: object;
  selectedTabTextStyle?: object;
  itemSpace?: number;
  currentRefinement?: any;
  refine?: any;
  isLoading?: boolean;
  onTabPress?: any;
  hasShadow?: boolean;
}

const styles = StyleSheet.create({
  tabsWrapStyle: {
    width: '100%',
    backgroundColor: 'white',
  },
  tabStyle: {
    justifyContent: 'center',
    paddingLeft: Metrics.smallMargin,
    paddingRight: Metrics.smallMargin,
    borderBottomWidth: 1,
    borderBottomColor: 'transparent'
  },
  selectedTabStyle: {
    borderBottomColor: Colors.accent,
  },
  tabTextStyle: {
    color: Colors.gray,
    fontSize: Fonts.size.medium
  },
  selectedTabTextStyle: {
    color: Colors.accent,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,

    elevation: 2,
  },
})

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

class CustomTabs extends Component<ICustomTabsProps, ICustomTabsState> {

  tabsWrapRef: React.RefObject<any>;

  constructor(props: any) {
    super(props);
    this.tabsWrapRef = React.createRef();

    this.state = {
      selectedTabId: 0,
      tabWidths: [],
      dataSelected: []
    }
  }

  static defaultProps = {
    type: 'business',
    height: Metrics.customTabsHeight,
    tabsWrapStyle: styles.tabsWrapStyle,
    tabStyle: styles.tabStyle,
    selectedTabStyle: styles.selectedTabStyle,
    tabTextStyle: styles.tabTextStyle,
    selectedTabTextStyle: styles.selectedTabTextStyle,
    itemSpace: 10,
    isLoading: false,
    hasShadow: true
  }
  // choosing what type of tabs to display
  async componentDidUpdate(prevProps: any) {
    if (prevProps.data !== this.props.data) {
      await this.setState({ dataSelected: this.props.data })
    }
  }

  /**
   * Save in state each Tab width, which is later used to move
   * them in the middle of the screen if possible.
   */
  public _setTabWidth = (event: any) => {
    this.setState({
      tabWidths: this.state.tabWidths.concat([event.nativeEvent.layout.width])
    });
  }

  /**
   * 1- Initially set the selected tab id to state.
   * 2- Check if InstantSearch props exist.
   * 3- Check if onPress prop exist
   * 4- Move the tabs.
   * @param selectedTabId
   */
  public _handleTabPress = async (arg: number) => {
    await this.setState({ selectedTabId: arg });

    if (this.props.refine) {
      this.props.refine(this.state.dataSelected[arg].value)
    }
    if (this.props.onTabPress) {
      this.props.onTabPress(arg);
    }
    // move the selected tab to middle if enough space
    this.tabsWrapRef.scrollToIndex({ index: arg, viewOffset: (initialLayout.width / 2) - this.state.tabWidths[arg] / 2 });
  }

  public _renderItem = ({ item, index }: any) => {
    const { height, tabStyle, selectedTabStyle, tabTextStyle, selectedTabTextStyle } = this.props
    const { selectedTabId } = this.state;
    return (
      <TouchableItem
        onLayout={this._setTabWidth}
        style={[tabStyle, { height: height }, index === selectedTabId ? selectedTabStyle : null]}
        onPress={this._handleTabPress.bind(this, index)}
      >
        <Text
          style={[tabTextStyle, index === selectedTabId ? selectedTabTextStyle : null]}
        >
          {item.label}
        </Text>
      </TouchableItem>
    );
  }

  public render() {

    const { tabsWrapStyle, itemSpace, isLoading, hasShadow } = this.props
    const { dataSelected } = this.state

    return (
      <View>
        {
          dataSelected.length === 0 ?
            null
            : isLoading ?
              <LoadingAnimatedMenu />
              : <FlatList
                style={[tabsWrapStyle, hasShadow ? styles.shadow : null]}
                keyExtractor={(item:any, index:number) => `${item.id}-${index}`}
                data={dataSelected}
                renderItem={this._renderItem}
                horizontal={true}
                ListFooterComponent={() => <View style={{ width: Metrics.smallMargin, height: '100%' }} />}
                ListHeaderComponent={() => <View style={{ width: Metrics.smallMargin, height: '100%' }} />}
                ItemSeparatorComponent={() => <View style={{ width: itemSpace, height: '100%' }} />}
                showsHorizontalScrollIndicator={false}
                ref={(ref: any) => { this.tabsWrapRef = ref }}
                {...this.props}
              />
        }
        {
          dataSelected.length > 0 ?
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: 6,
                bottom: -6,
                left: 0,
                backgroundColor: 'white',
              }}
            />
            : null
        }
      </View>
    );
  }
}

export default CustomTabs;

/**
 * Customized library to make horizontal elements.
 * Original usage was very limited and had to wrap and
 * customize in order to fit the needs.
 */
const LoadingAnimatedMenu = () => (
  <Placeholder
    style={{
      height: Metrics.customTabsHeight,
      padding: 5,
      justifyContent: 'center',
      alignContent: 'center',
      backgroundColor: 'white',
    }}
    Animation={ShineOverlay}
  >
    <View
      style={{
        width: initialLayout.width,
        flexDirection: 'row',
      }}>
      <View>
        <PlaceholderLine
          style={{ width: 60, margin: 5 }}
          color={'#F62C8B30'}
          width={100}
        />
      </View>
      <View>
        <PlaceholderLine
          style={{ width: 90, margin: 5 }}
          color={'#F62C8B30'}
          width={40}
        />
      </View>
      <View>
        <PlaceholderLine
          style={{ width: 60, margin: 5 }}
          color={'#F62C8B30'}
          width={40}
        />
      </View>
      <View>
        <PlaceholderLine
          style={{ width: 50, margin: 5 }}
          color={'#F62C8B30'}
          width={40}
        />
      </View>
      <View>
        <PlaceholderLine
          style={{ width: 90, margin: 5 }}
          color={'#F62C8B30'}
          width={40}
        />
      </View>
    </View>
  </Placeholder>
);