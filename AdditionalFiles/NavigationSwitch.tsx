import React, { useContext, useEffect } from 'react';
import { View, ActivityIndicator, StatusBar } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { NavigationParams, NavigationScreenProp, NavigationState } from 'react-navigation';
import { UserContext } from '../Provider/UserProvider';
//
import { Colors } from '../Themes'

export interface BootstrapScreenProps {
	navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const BootstrapScreen = ({ navigation }: BootstrapScreenProps) => {

	const { isUserLogged } = useContext(UserContext)

	useEffect(
		() => {
			let mounted = false;
			if (isUserLogged === true) {
				navigation.navigate('User')
			} else if (isUserLogged === false) {
				navigation.navigate('Guest')
			}
			return () => {
				mounted = true
			}
		}, [isUserLogged]
	);

	return (
		<View
			style={{
				flex: 1,
				backgroundColor: Colors.lightGray,
				alignItems: 'center',
				justifyContent: 'center'
			}}
		>
			<StatusBar barStyle="light-content" backgroundColor={Colors.darkGrey} />
			<ActivityIndicator size="large" color={Colors.darkGrey} />
		</View>
	);
}

const BootstrapRouter =
	createStackNavigator(
		{
			Main: BootstrapScreen
		}, {
		headerMode: 'none',
		initialRouteName: 'Main'
	}
	)

export default BootstrapRouter