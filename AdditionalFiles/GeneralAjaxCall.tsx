function getDailyBilance() {
	setGeneralLoader(true)
	const path = `/delivery/bilance`
	let data = new FormData();
	data.append("date", formatDate(ordersFilterDate));
	Api.postAuth(path, userToken, data)
		.then(
			(response: any) => {
				console.log(response);
				let responseStatus = ResponseStatus.handleResponse(response);
				if (response.status !== 401) {
					if (responseStatus === true) {
						setDailyBilanceInfo(response.data)
						setGeneralLoader(false)
					} else {
						Alert.alert(
							strLng.LABELS.warning,
							strLng.ERROR.problem_during_daily_activity,
							[
								{
									text: strLng.ACTIONS.report,
									onPress: () => reportProblemMain(`${'{put}'}${path}`, JSON.stringify(response.data))
								},
								{ text: strLng.ACTIONS.retry, onPress: () => getDailyBilance() },
							],
							{ cancelable: true },
						);
						setGeneralLoader(false);
					}
				} else {
					Core.removeToken()
				}
			}
		)
		.catch(
			() => {
				generalMessage(strLng.ERROR.error)
				setGeneralLoader(false)
			}
		);
}