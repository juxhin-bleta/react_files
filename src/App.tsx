import React from 'react';
import RootContainer from './container/RootContainer'

interface AppProps {

}

function App() {
  return (
    <div className="media_world_register">
      <RootContainer />
    </div>
  );
}

export default App;
